//
//  STTask.h
//  StudyTime
//
//  Created by Davindra Sutraban on 5/9/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STTask : NSObject
{
    //NSInteger numPomodoros;
}

@property (nonatomic, copy) NSString *taskName;
@property (nonatomic) NSDate *beginTime;
@property (nonatomic) NSDate *endTime;

- (instancetype)initWithTaskName:(NSString *)n;

@end
