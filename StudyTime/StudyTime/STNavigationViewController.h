//
//  STNavigationViewController.h
//  StudyTime
//
//  Created by Davindra Sutraban on 5/10/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STNavigationViewController : UINavigationController

@end
