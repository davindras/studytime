//
//  STDetailViewController.m
//  StudyTime
//
//  Created by Davindra Sutraban on 5/13/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import "STDetailViewController.h"

@interface STDetailViewController ()

@end

@implementation STDetailViewController
@synthesize taskName, taskChange, statusLabel, ind;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        UINavigationItem *navItem = [[UINavigationItem alloc] init];
        navItem.title = @"Task Detail";
    }
    return self;
}

- (instancetype)initWithTaskName:(NSString *)task index:(NSUInteger)taskIndex
{
    self = [super init];
    if (self) {
        taskName = task;
        ind = taskIndex;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    taskChange.text = taskName;
    statusLabel.text = @"Not Started";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


# pragma mark - UITextField Delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *changedTask = textField.text;
    
    if (changedTask == taskName) {
        [[self navigationController] popToRootViewControllerAnimated:YES];
        return YES;
    }else{
        [self.delegate didChangeTask:self withTaskName:changedTask index:ind];
        [[self navigationController] popToRootViewControllerAnimated:YES];
        return YES;
    }
}

@end
