//
//  STTaskTableViewController.h
//  StudyTime
//
//  Created by Davindra Sutraban on 5/8/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STDetailViewController.h"

@interface STTaskTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, STDetailViewControllerDelegate>
{
}

@property IBOutlet UITextField *taskField;

@end
