//
//  STSettingsViewController.h
//  StudyTime
//
//  Created by Davindra Sutraban on 5/15/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STSettingsViewController : UIViewController <UITextViewDelegate>

- (IBAction)setFocusTime:(id)sender;
- (IBAction)setShortBreak:(id)sender;
- (IBAction)setLongBreak:(id)sender;
- (IBAction)beginFocus:(id)sender;

@end
