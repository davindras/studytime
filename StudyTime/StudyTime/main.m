//
//  main.m
//  StudyTime
//
//  Created by Davindra Sutraban on 5/8/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "STAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([STAppDelegate class]));
    }
}
