//
//  STDetailViewController.h
//  StudyTime
//
//  Created by Davindra Sutraban on 5/13/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <UIKit/UIKit.h>
@class STDetailViewController;

@protocol STDetailViewControllerDelegate <NSObject>

- (void)didChangeTask:(STDetailViewController *)controller withTaskName:(NSString *)name index:(NSUInteger)ind;

@end

@interface STDetailViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, weak) id <STDetailViewControllerDelegate> delegate;
@property (nonatomic, copy) NSString *taskName;
@property (weak, nonatomic) IBOutlet UITextField *taskChange;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (nonatomic) NSUInteger ind;

- (instancetype)initWithTaskName:(NSString *)task index:(NSUInteger)taskIndex;

@end
