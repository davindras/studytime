//
//  STTimerViewController.m
//  StudyTime
//
//  Created by Davindra Sutraban on 6/21/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import "STTimerViewController.h"
#import "STTimer.h"
#import "STTaskList.h"
#import "STTask.h"

@interface STTimerViewController ()

@property int numTimerFired;

@end

@implementation STTimerViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    STTimer *time = [STTimer sharedTimer];
    //NSInteger numSeconds = (time.focusTime) * 60;
    _numTimerFired = 0;
    time.timeRunning = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerTest:) userInfo:nil repeats:YES];
    _timerLabel.text = [@(time.focusTime) stringValue];
    
    // Setting up to show the task at the bottom
    NSArray *tasks = [[STTaskList sharedTaskList] allTasks];
    STTask *curTask = [tasks objectAtIndex:0];
    _taskLabel.text = curTask.taskName;
    _taskLabel.numberOfLines = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)stopTaskTimer:(id)sender {
    
    // Check GoogleDrive/FocusTime/TechnicalStuff
}

- (IBAction)pauseTaskTimer:(id)sender {
    
    UIButton *pauseButton = sender;
    NSLog(@"%@", pauseButton.titleLabel);
    if ([[pauseButton titleLabel].text isEqual:@"Pause"]) {
        [pauseButton setTitle:@"Play" forState:UIControlStateNormal];
    }else{
        [pauseButton setTitle:@"Pause" forState:UIControlStateNormal];
    }
    
    NSLog(@"We are clicking the pause button.");
}

- (IBAction)skipCurrentTask:(id)sender {

    // Check GoogleDrive/FocusTime/TechnicalStuff
}

- (void)timerTest:(NSTimer *)timer
{
    
    _numTimerFired++;
    _timerLabel.text = [@(_numTimerFired) stringValue];
    if (_numTimerFired == 5) {
        NSLog(@"The timer ran 5 times and show be 25 seconds.");
        [timer invalidate];
    }
}
@end
