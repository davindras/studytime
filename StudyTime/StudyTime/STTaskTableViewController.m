//
//  STTaskTableViewController.m
//  StudyTime
//
//  Created by Davindra Sutraban on 5/8/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import "STTaskTableViewController.h"
#import "STTaskList.h"
#import "STTask.h"
#import "STDetailViewController.h"
#import "STSettingsViewController.h"

@interface STTaskTableViewController ()

@end

@implementation STTaskTableViewController
@synthesize taskField;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        
        UINavigationItem *navItem = self.navigationItem;
        navItem.title = @"Study Time";
        navItem.leftBarButtonItem = self.editButtonItem;
        UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay target:self action:@selector(proceed)];
        navItem.rightBarButtonItem = rightItem;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Task"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[STTaskList sharedTaskList] allTasks] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Task" forIndexPath:indexPath];
    
    NSInteger taskIndex = indexPath.row;
    STTask *currentTask = [[[STTaskList sharedTaskList] allTasks] objectAtIndex:taskIndex];
    cell.textLabel.text = currentTask.taskName;
    
    return cell;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[STTaskList sharedTaskList] deleteTask:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    [[STTaskList sharedTaskList] moveTask:fromIndexPath.row :toIndexPath.row];
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


#pragma mark - Header View

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *foot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 0)];
    taskField = [[UITextField alloc] initWithFrame:CGRectMake(10, 14, tableView.bounds.size.width - 20, 50)];
    taskField.placeholder = @"Enter task here";
    taskField.borderStyle = UITextBorderStyleNone;
    taskField.delegate = self;
    [foot addSubview:taskField];

    [taskField becomeFirstResponder];
    
    return foot;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80;
}


#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *taskName = textField.text;
    NSInteger index = [[STTaskList sharedTaskList] createTaskWithTaskName:taskName];

    NSIndexPath *iPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[iPath] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
    
    textField.text = @"";
    return YES;
}

# pragma mark - Selecting a Row
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSString *taskName = [[[[STTaskList sharedTaskList] allTasks] objectAtIndex:indexPath.row] taskName];

    STDetailViewController *detail = [[STDetailViewController alloc] initWithTaskName:taskName index:indexPath.row];
    detail.delegate = self;
    [[self navigationController] pushViewController:detail animated:YES];
}

# pragma mark - STDetailViewControllerDelegate method
- (void)didChangeTask:(STDetailViewController *)controller withTaskName:(NSString *)name index:(NSUInteger)ind
{
    [[STTaskList sharedTaskList] renameTaskAtIndex:ind withName:name];
    [self.tableView reloadData];
}

// This method will be called when the user is ready to study and begin the list
- (void)proceed
{
    if ([[[STTaskList sharedTaskList] allTasks] count] == 0) {
        return;
    }else{
        STSettingsViewController *svc = [[STSettingsViewController alloc] init];
        [[self navigationController] pushViewController:svc animated:YES];
    }
    
}


@end
