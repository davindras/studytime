//
//  STTaskList.h
//  StudyTime
//
//  Created by Davindra Sutraban on 5/9/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <Foundation/Foundation.h>
@class STTask;

@interface STTaskList : NSObject
{
    //NSMutableArray *taskArray;
}

@property (nonatomic) NSMutableArray *taskArray;

+ (instancetype)sharedTaskList;

- (STTask *)createTask;
- (NSInteger)createTaskWithTaskName:(NSString *)tName;
- (NSArray *)allTasks;
- (void)deleteTask:(NSUInteger)deleteIndex;
- (void)moveTask:(NSUInteger)fromIndex
                :(NSUInteger)toIndex;
- (void)renameTaskAtIndex:(NSUInteger)index withName:(NSString *)name;
@end
