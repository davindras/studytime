//
//  STTimer.m
//  StudyTime
//
//  Created by Davindra Sutraban on 6/13/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import "STTimer.h"

@implementation STTimer

+ (instancetype)sharedTimer
{
    static STTimer *taskTimer = nil;
    if (!taskTimer) {
        taskTimer = [[self alloc] init];
    }
    
    return taskTimer;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _focusTime = 0;
        _shortBreakTime = 0;
        _longBreakTime = 0;
        _numFocusPeriods = 0;
        _numShortPeriods = 0;
        _numLongPeriods = 0;
        //_timeRunning = [[NSTimer alloc] init];
    }
    
    return self;
}

// Method that can modify the focusTime, shortBreakTime, and longBreakTime
- (void)changeValueFor:(NSInteger)type :(NSInteger)value
{
    switch (type) {
        case 1:
            _focusTime = value;
            break;
        case 2:
            _shortBreakTime = value;
            break;
        case 3:
            _longBreakTime = value;
        default:
            break;
    }
}
@end
