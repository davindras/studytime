//
//  STTimer.h
//  StudyTime
//
//  Created by Davindra Sutraban on 6/13/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STTimer : NSObject

@property (nonatomic) NSInteger focusTime;
@property (nonatomic) NSInteger shortBreakTime;
@property (nonatomic) NSInteger longBreakTime;
@property (nonatomic) NSInteger numFocusPeriods;
@property (nonatomic) NSInteger numShortPeriods;
@property (nonatomic) NSInteger numLongPeriods;
@property (nonatomic) NSTimer *timeRunning;

+ (instancetype)sharedTimer;
//- (NSTimer *)createTimer;
- (void)changeValueFor:(NSInteger)type
                      :(NSInteger)value;




@end
