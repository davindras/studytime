//
//  STSettingsViewController.m
//  StudyTime
//
//  Created by Davindra Sutraban on 5/15/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import "STSettingsViewController.h"
#import "STTimer.h"
#import "STTimerViewController.h"

@interface STSettingsViewController ()

@end

@implementation STSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)setFocusTime:(id)sender {
    
    UISegmentedControl *ctrlFocus = sender;
    
    if (ctrlFocus.selectedSegmentIndex == 0) {
        [[STTimer sharedTimer] changeValueFor:1 :25];
    } else if (ctrlFocus.selectedSegmentIndex == 1){
        [[STTimer sharedTimer] changeValueFor:1 :35];
    } else if(ctrlFocus.selectedSegmentIndex == 2){
        [[STTimer sharedTimer] changeValueFor:1 :45];
    } else {
        NSLog(@"Oh man, we got issues here");
    }
    
}

- (IBAction)setShortBreak:(id)sender {
    
    UISegmentedControl *ctrlShort = sender;
    
    if (ctrlShort.selectedSegmentIndex == 0) {
        [[STTimer sharedTimer] changeValueFor:2 :3];
    } else if (ctrlShort.selectedSegmentIndex == 1){
        [[STTimer sharedTimer] changeValueFor:2 :5];
    } else if(ctrlShort.selectedSegmentIndex == 2){
        [[STTimer sharedTimer] changeValueFor:2 :7];
    } else{
        NSLog(@"Issues in setShortBreak method");
    }
    
}

- (IBAction)setLongBreak:(id)sender {
    
    UISegmentedControl *ctrlLong = sender;
    
    if (ctrlLong.selectedSegmentIndex == 0) {
        [[STTimer sharedTimer] changeValueFor:3 :20];
    } else if (ctrlLong.selectedSegmentIndex == 1){
        [[STTimer sharedTimer] changeValueFor:3 :30];
    } else if(ctrlLong.selectedSegmentIndex == 2){
        [[STTimer sharedTimer] changeValueFor:3 :40];
    } else{
        NSLog(@"Issues in setLongBreak method");
    }
    
}

- (IBAction)beginFocus:(id)sender {
    
    //NSLog(@"The focus time is %ld minutes \nThe short break time is %ld minutes \nThe long break time is %ld minutes",
    //      (long)[[STTimer sharedTimer] focusTime], (long)[[STTimer sharedTimer] shortBreakTime], (long)[[STTimer sharedTimer] longBreakTime]);
    
    STTimerViewController *tvc = [[STTimerViewController alloc] init];
    [self.navigationController pushViewController:tvc animated:YES];
}


@end
