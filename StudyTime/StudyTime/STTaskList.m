//
//  STTaskList.m
//  StudyTime
//
//  Created by Davindra Sutraban on 5/9/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import "STTaskList.h"
#import "STTask.h"

@implementation STTaskList
@synthesize taskArray;

+ (instancetype)sharedTaskList
{
    static STTaskList *taskList = nil;
    if (!taskList) {
        taskList = [[self alloc] init];
    }
    
    return taskList;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        taskArray = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (STTask *)createTask
{
    STTask *task = [[STTask alloc] init];
    [taskArray addObject:task];
    return task;
}

- (NSInteger)createTaskWithTaskName:(NSString *)tName
{
    STTask *task = [[STTask alloc] initWithTaskName:tName];
    [taskArray addObject:task];
    NSInteger index = [taskArray indexOfObject:task];
    
    return index;
}

- (NSArray *)allTasks
{
    return taskArray;
}

- (void)deleteTask:(NSUInteger)deleteIndex
{
    [taskArray removeObjectAtIndex:deleteIndex];
}

- (void)moveTask:(NSUInteger)fromIndex
                :(NSUInteger)toIndex
{
    STTask *temp = [taskArray objectAtIndex:fromIndex];
    [taskArray removeObjectAtIndex:fromIndex];
    [taskArray insertObject:temp atIndex:toIndex];
}

- (void)renameTaskAtIndex:(NSUInteger)index withName:(NSString *)name
{
    STTask *updateTask = [[[STTaskList sharedTaskList] allTasks] objectAtIndex:index];
    updateTask.taskName = name;
    [[[STTaskList sharedTaskList] taskArray] removeObjectAtIndex:index];
    [[[STTaskList sharedTaskList] taskArray] insertObject:updateTask atIndex:index];
}

@end
