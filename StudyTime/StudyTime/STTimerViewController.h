//
//  STTimerViewController.h
//  StudyTime
//
//  Created by Davindra Sutraban on 6/21/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STTimerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskLabel;

- (IBAction)stopTaskTimer:(id)sender;
- (IBAction)pauseTaskTimer:(id)sender;
- (IBAction)skipCurrentTask:(id)sender;
- (void)timerTest:(NSTimer *)timer;

@end
