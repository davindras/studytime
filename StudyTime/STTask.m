//
//  STTask.m
//  StudyTime
//
//  Created by Davindra Sutraban on 5/9/14.
//  Copyright (c) 2014 Davindra Sutraban. All rights reserved.
//

#import "STTask.h"

@implementation STTask

@synthesize taskName, beginTime, endTime;

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        beginTime = [[NSDate alloc] init];
        endTime = [[NSDate alloc] init];
        taskName = @"";
        
    }
    
    return self;
}

- (instancetype)initWithTaskName:(NSString *)n
{
    self = [super init];
    if (self) {
        
        beginTime = [[NSDate alloc] init];
        endTime = [[NSDate alloc] init];
        taskName = n;
        
    }
    
    return self;
}


@end
